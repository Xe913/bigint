#pragma once
#include <iostream>
#include "BigInt.h"

#define TEST(FUNC, NAME) std::cout << NAME << " result\t" << (FUNC() ? "SUCCESS" : "FAILED") << std::endl;

bool UnaryOperatorsTest()
{
	BigInt a( "911111111119991111");
	BigInt b( "222222222222222222");
	BigInt c("1133333333342213333");
	BigInt d("-500000000000000000");
	BigInt e("-300000000000000000");
	BigInt f("-200000000000000000");
	BigInt g("10");
	BigInt h("100");
	BigInt i("1000");
	BigInt j("10000000000000000000001");
	BigInt k("1000000000000000000000");
	BigInt l("10");
	BigInt m("10000000000000000000");
	BigInt n( "9999999999999999667");
	BigInt o("333");
	a += b;
	d -= e;
	g *= h;
	j /= k;
	m %= n;
	return (a == c) && (d == f) && (g == i) && (j == l) && (m == o);
}

bool BinaryOperatorsTest()
{
	BigInt a("100000000000000000");
	BigInt b("200000000000000000");
	BigInt c("300000000000000000");
	BigInt d("500000000000000000");
	BigInt e("700000000000000000");
	BigInt f("-200000000000000000");
	BigInt g("10000000");
	BigInt h("20000000");
	BigInt i("200000000000000");
	BigInt j("1000");
	BigInt k("101");
	BigInt l("9");
	BigInt m("91");
	return (a + b == c) && (d - e == f) && (g * h == i) && (j / k == l) && (j % k == m);
}

bool ShiftTest()
{
	BigInt a("100000000000000");
	BigInt b("2");
	BigInt c("25000000000000");
	BigInt d("100000000000000");
	BigInt e("3");
	BigInt f("800000000000000");
	BigInt g("100000000000000");
	BigInt h("2");
	BigInt i("25000000000000");
	BigInt j("100000000000000");
	BigInt k("3");
	BigInt l("800000000000000");
	a >>= b;
	d <<= e;

	return (a == c) && (d == f) && (g >> h == i) && (j << k == l);
}

int main()
{
	TEST(UnaryOperatorsTest, "Unary Operators Tests")
	TEST(BinaryOperatorsTest, "Binary Operators Tests")
	TEST(ShiftTest, "Shift Operators Tessts")
}