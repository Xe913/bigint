#pragma once
#include "BigInt.h"
#include <string>
#include <vector>
#include <inttypes.h>

constexpr uint64_t max_power10_in_max_power2[] = { 0, 100, 10000, 10000000, 1000000000, 1000000000000, 100000000000000, 10000000000000000, 10000000000000000000 };

constexpr uint64_t max_power10()
{
	return max_power10_in_max_power2[sizeof(uint32_t)];
}

template <uint64_t T>
struct count_digits
{
	static constexpr uint64_t value = 1 + count_digits<T / 10>::value;

};

template <>
struct count_digits<0>
{
	static constexpr uint64_t value = 0;
};

constexpr uint64_t max_digits()
{
	return count_digits<max_power10()>::value;
}

BigInt::BigInt()
	: data(1), negSign(false) { }

BigInt::BigInt(int32_t n) : data(1)
{
	if (n < 0) {
		negSign = true;
		n = -n;
	}
	else
	{
		negSign = false;
	}
	data[0] = n;
}

BigInt::BigInt(const BigInt& other)
	: data(other.data), negSign(other.negSign) { }

BigInt::BigInt(BigInt&& other) noexcept
	: data(std::move(other.data)), negSign(other.negSign) { }

BigInt::BigInt(const std::string& nAsString) : BigInt()
{
	if (nAsString.length() == 0)
		return;

	if (nAsString.c_str()[0] == '-')
		negSign = true;

	for (size_t i = negSign ? 1 : 0; i < nAsString.length(); ++i)
	{
		const char digitAsChar = nAsString.c_str()[i];
		const int32_t digit = digitAsChar - '0';

		*this *= 10;
		Sum(digit);
	}
}

BigInt::~BigInt() { }

uint32_t BigInt::uint_union::Rightmost() const {
	return value;
}

uint32_t BigInt::uint_union::Leftmost() const {
	return value >> sizeof(uint32_t) * CHAR_BIT;
}

uint32_t BigInt::sint_union::Positive() const
{
	if (value >= 0)
		return value;
	else
		return std::numeric_limits<uint32_t>::max() + (value + (int64_t)1);
}

BigInt::operator uint32_t() const { return data[0]; }

short BigInt::CompareAbs(const BigInt& other) const
{
	if (data.size() != other.data.size())
		return data.size() > other.data.size() ? 1 : -1;

	//TODO see if can be done withou index
	for (size_t i = 0; i < data.size(); ++i)
	{
		const size_t index = data.size() - i - 1;
		if (data[index] != other.data[index])
			return data[index] > other.data[index] ? 1 : -1;
	}

	return 0;
}

//sum the same position values in the two deques and the stored carry,
//than use the left part of the 64 bit value as carry for the next cycle
//Note: this method is used to add a quantity to another. Its called
//in a sum between values the same sign and in a subtraction between
//values with different signs
void BigInt::Sum(const BigInt& other)
{
	const size_t maxLen = std::max<size_t>(data.size(), other.data.size());
	const uint32_t zero = (uint32_t)0;
	data.resize(maxLen);
	uint32_t rest{ 0 };

	for (size_t i = 0; i < maxLen; ++i)
	{
		const uint_union op1 = { data[i] };
		const uint_union op2 = { i < other.data.size() ? other.data[i] : zero };
		const uint_union sum = { op1.value + op2.value + rest };
		data[i] = sum.Rightmost();
		rest = sum.Leftmost();
	}

	if (rest > 0)
		data.push_back(rest);
}

//subtract the same position values in the two deques, also subtract -1
//if the previous cycle result was negative
//Note: this method is used to subtract a quantity from another. Its called
//in a sum between values with different signs and in a subtraction between
//values with the same sign
void BigInt::Sub(const BigInt& other)
{
	int64_t rest{ 0 };
	const uint32_t zero = (uint32_t)0;

	const size_t maxLenght = std::max<size_t>(data.size(), other.data.size());
	data.resize(maxLenght);

	//to perform subtraction, the operator with the largest absolute value
	//is considered positive
	const int64_t op1sign = CompareAbs(other) >= 0 ? 1 : -1;
	//and the other is subtracted from it
	const int64_t op2sign = -op1sign;

	for (size_t i = 0; i < maxLenght; ++i)
	{
		const sint_union a = { data[i] };
		const sint_union b = { i < other.data.size() ? other.data[i] : zero };

		const sint_union sum = { op1sign * a.value + op2sign * b.value + rest };

		data[i] = sum.Positive(); 
		rest = sum.value >= 0 ? 0 : -1;
	}

	while (data.back() == 0 && data.size() > 1)
		data.pop_back();

	//was positive and was subtracted a smaller number -> the result is positive
	//was positive and was subtracted a larger number -> the result is negative
	//was negative and was subtracted a smaller number -> the result is negative
	//was negative and was subtracted a larger number -> the result is positive
	negSign = (negSign == (op1sign > 0));
}

//utility "switch" to try different product methods
void BigInt::Product(const BigInt& other)
{
	Multiply(other);
	//PaesantMultiply(other);
}

//multiply the same position values in the two deques, add the previous
//cycle carry, use the left part of the 64 bit value to stor the carry
//for the next cycle
BigInt BigInt::Multiply(const BigInt& other)
{
	negSign = (negSign != other.negSign);
	uint32_t rest{ 0 };

	const size_t resultLen = data.size() + other.data.size();
	data_t result(resultLen - 1);

	for (size_t i = 0; i < data.size(); i++)
	{
		const uint_union op1 = { data[i] };
		for (size_t j = 0; j < other.data.size(); j++)
		{
			const uint_union op2 = { other.data[j] };
			const uint_union prod = { op1.value * op2.value + rest };

			result[i + j] += prod.Rightmost();
			rest = prod.Leftmost();
		}
	}
	if (rest != 0)
		result.push_back(rest);
	data = std::move(result);
	return *this;
}

//doesn't work
//TODO: make it work
BigInt BigInt::PaesantMultiply(const BigInt& other)
{
	BigInt res;
	if (CompareAbs(other) < 0)
		res = PaesantMultiplicationStep(*this, other);
	res = PaesantMultiplicationStep(other, *this);
	res.negSign = (negSign != other.negSign);
	return res;
}

BigInt BigInt::PaesantMultiplicationStep(const BigInt& op1, const BigInt& op2)
{
	BigInt first{ op1 };
	first.negSign = false;
	BigInt second{ op2 };
	second.negSign = false;
	BigInt res;
	BigInt rest;
	while (first > 1)
	{
		rest = first % 2;
		first /= 2;
		second += second;
		if (rest != 0) res += second;
	}
	return res;
}

//utility "switch" to try different product methods
BigInt BigInt::Division(const BigInt& other)
{
	return CyclicSubtraction(other);
}

//naive method that performs division by successive
//subtractions. Really slow with big numbers
//TODO: make a better method work
BigInt BigInt::CyclicSubtraction(const BigInt& other)
{
	bool resNegSign = negSign != other.negSign;
	BigInt divisor = other;
	BigInt resData;

	negSign = false;
	divisor.negSign = false;

	while (CompareAbs(other) >= 0)
	{
		Sub(divisor);
		++resData;
	}

	BigInt rest{ *this };
	resData.negSign = resNegSign;
	data = resData.data;
	return rest;
}

void BigInt::ShiftLeft(uint32_t n)
{
	uint32_t rest{ 0 };

	for (size_t i = 0; i < data.size(); i++)
	{
		uint_union a = { data[i] };
		a.value <<= n;
		a.value |= rest;
		data[i] = a.Rightmost();
		rest = a.Leftmost();
	}

	if (rest > 0)
		data.push_back(rest);
}

void BigInt::ShiftRight(uint32_t n)
{
	uint32_t rest{ 0 };
	const uint32_t maxShift = sizeof(uint32_t) * CHAR_BIT;

	for (size_t i = 0; i < data.size(); i++)
	{
		const size_t index = data.size() - i - 1;
		uint_union shifted = { data[index] };
		shifted.value <<= maxShift;
		shifted.value >>= n;
		data[index] = shifted.Leftmost();
		data[index] |= rest;
		rest = shifted.Rightmost();
	}

	if (data.back() == 0 && data.size() > 1)
		data.pop_back();
}

BigInt& BigInt::operator=(const BigInt& other) {
	data = other.data;
	negSign = other.negSign;

	return *this;
}

BigInt& BigInt::operator=(BigInt&& other) noexcept {
	std::swap(data, other.data);
	negSign = other.negSign;

	return *this;
}

BigInt& BigInt::operator+=(const BigInt& other)
{
	if (negSign == other.negSign)
		Sum(other);
	else
		Sub(other);

	return *this;
}

BigInt& BigInt::operator-=(const BigInt& other)
{
	if (negSign != other.negSign)
		Sum(other);
	else
		Sub(other);

	return *this;
}

BigInt& BigInt::operator*=(const BigInt& other)
{
	Product(other);
	return *this;
}

BigInt& BigInt::operator/=(const BigInt& other)
{
	Division(other);
	return *this;
}

BigInt& BigInt::operator%=(const BigInt& other)
{
	data = Division(other).data;
	negSign = false;
	return *this;
}

BigInt operator+(const BigInt& op1, const BigInt& op2)
{
	return BigInt{ op1 } += op2;
}

BigInt operator-(const BigInt& op1, const BigInt& op2)
{
	return BigInt{ op1 } -= op2;
}

BigInt operator*(const BigInt& op1, const BigInt& op2)
{
	return BigInt{ op1 } *= op2;
}

BigInt operator/(const BigInt& op1, const BigInt& op2)
{
	return BigInt{ op1 } /= op2;
}

BigInt operator%(const BigInt& op1, const BigInt& op2)
{
	return BigInt{ op1 } %= op2;
}

BigInt& BigInt::operator++()
{
	*this += 1;
	return *this;
}

BigInt BigInt::operator++(int)
{
	BigInt res(*this);
	++(*this);
	return res;
}

BigInt& BigInt::operator--()
{
	*this -= 1;
	return *this;
}

BigInt BigInt::operator--(int)
{
	BigInt res(*this);
	--(*this);
	return res;
}

bool operator==(const BigInt& op1, const BigInt& op2)
{
	return op1.CompareAbs(op2) == 0;
}

bool operator!=(const BigInt& op1, const BigInt& op2)
{
	return !(op1 == op2);
}

bool operator<(const BigInt& a, const BigInt& b)
{
	if (a.negSign != b.negSign) return a.negSign;

	if (a.negSign) return a.CompareAbs(b) > 0;
	else return a.CompareAbs(b) < 0;
}

bool operator>(const BigInt& a, const BigInt& b)
{
	return b < a;
}

bool operator<=(const BigInt& a, const BigInt& b)
{
	return !(a > b);
}

bool operator>=(const BigInt& a, const BigInt& b)
{
	return !(a < b);
}

BigInt& BigInt::operator&=(const BigInt& other)
{
	const uint32_t zero = (uint32_t)0;
	for (size_t i = 0; i < data.size(); i++)
	{
		const uint32_t otherValue = { i < other.data.size() ? other.data[i] : zero };
		data[i] &= otherValue;
	}
	return *this;
}

BigInt& BigInt::operator<<=(const BigInt& other)
{
	BigInt otherTemp{ other };
	const uint32_t maxShift = sizeof(uint32_t) * CHAR_BIT;

	while (otherTemp > 0)
	{
		uint32_t shifted{ 0 };
		if (otherTemp >= maxShift)
			shifted = maxShift;
		else
			shifted = (uint32_t)otherTemp;

		otherTemp -= maxShift;
		ShiftLeft(shifted);
	}

	return *this;
}

BigInt& BigInt::operator>>=(const BigInt& other)
{
	BigInt otherTemp{ other };
	const uint32_t maxShift = sizeof(uint32_t) * CHAR_BIT;

	while (otherTemp > 0)
	{
		uint32_t shifted{ 0 };
		if (otherTemp >= maxShift)
			shifted = maxShift;
		else
			shifted = (uint32_t)otherTemp;

		otherTemp -= maxShift;
		ShiftRight(shifted);
	}
	return *this;
}

BigInt& BigInt::operator|=(const BigInt& other)
{
	const uint32_t zero = (uint32_t)0;
	for (size_t i = 0; i < data.size(); i++)
	{
		const uint32_t otherValue = { i < other.data.size() ? other.data[i] : zero };
		data[i] |= otherValue;
	}
	return *this;
}

BigInt& BigInt::operator^=(const BigInt& other)
{
	const uint32_t zero = (uint32_t)0;
	for (size_t i = 0; i < data.size(); i++)
	{
		const uint32_t otherValue = { i < other.data.size() ? other.data[i] : zero };
		data[i] ^= otherValue;
	}
	return *this;
}

BigInt operator&(const BigInt& op1, const BigInt& op2)
{
	return BigInt{ op1 } &= op2;
}

BigInt operator|(const BigInt& op1, const BigInt& op2)
{
	return BigInt{ op1 } |= op2;
}

BigInt operator^(const BigInt& op1, const BigInt& op2)
{
	return BigInt{ op1 } ^= op2;
}

BigInt operator~(const BigInt& bigInt)
{
	BigInt res{ bigInt };
	for (size_t i = 0; i < res.data.size(); i++)
		res.data[i] = ~res.data[i];
	return res;
}

BigInt operator<<(const BigInt& op1, const BigInt& op2)
{
	return BigInt{ op1 } <<= op2;
}

BigInt operator>>(const BigInt& op1, const BigInt& op2)
{
	return BigInt{ op1 } >>= op2;
}

std::string BigInt::ToStringFormatted(uint64_t num)
{
	constexpr uint64_t maxZeroes = max_digits() - 1;
	std::string format = "%0";
	format.append(std::to_string(maxZeroes));
	format.append(PRIu64);

	char buffer[maxZeroes + 1];
	sprintf_s(buffer, maxZeroes + 1, format.c_str(), num);

	return buffer;
}

std::ostream& operator<<(std::ostream& os, const BigInt& bInt)
{
	std::vector<std::string> result;

	BigInt a{ bInt };
	a.negSign = false;

	constexpr uint32_t divisor = max_power10();
	while (a > divisor)
	{
		const BigInt rest = a.Division(divisor);
		const std::string resultDigit = BigInt::ToStringFormatted(rest.data[0]);
		result.push_back(resultDigit);
	}

	const std::string resultDigit = std::to_string(a.data[0]); //Last rest
	result.push_back(resultDigit);

	if (bInt.negSign)
	{
		result.push_back("-");
	}

	std::reverse(result.begin(), result.end());

	for (size_t i = 0; i < result.size(); i++)
	{
		os << result[i];
	}

	return os;
}