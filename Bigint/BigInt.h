#pragma once
#include <iostream>
#include <deque>

using std::uint32_t;
using std::int32_t;
using std::uint64_t;
using std::int64_t;


/*
BigInt is represented by a deque of unsigned int32 and a bool
which is true if the number is negative

this means arithmetical operations must be performed by iterating
through the structure. 64 bit values are used to store a given
cycle rest or carry to use it in the next cycle
*/
class BigInt
{
public:
	typedef std::deque<uint32_t> data_t;

	struct uint_union
	{
		uint64_t value;

		uint32_t Rightmost() const;
		uint32_t Leftmost() const;
	};

	struct sint_union
	{
		int64_t value;

		uint32_t Positive() const;
		bool Sign() const;
	};
	
	BigInt();
	BigInt(int32_t);
	BigInt(const BigInt& other);
	BigInt(BigInt&& other) noexcept;
	BigInt(const std::string&);
	virtual ~BigInt();
	
	BigInt& operator=(const BigInt& other);
	BigInt& operator=(BigInt&& other) noexcept;

	explicit operator uint32_t() const;
	BigInt& operator+=(const BigInt& other);
	BigInt& operator-=(const BigInt& other);
	BigInt& operator*=(const BigInt& other);
	BigInt& operator/=(const BigInt& other);
	BigInt& operator%=(const BigInt& other);

	BigInt& operator++();
	BigInt operator++(int);
	BigInt& operator--();
	BigInt operator--(int);

	friend BigInt operator+(const BigInt& op1, const BigInt& op2);
	friend BigInt operator-(const BigInt& op1, const BigInt& op2);
	friend BigInt operator*(const BigInt& op1, const BigInt& op2);
	friend BigInt operator/(const BigInt& op1, const BigInt& op2);
	friend BigInt operator%(const BigInt& op1, const BigInt& op2);

	friend bool operator==(const BigInt& op1, const BigInt& op2);
	friend bool operator!=(const BigInt& op1, const BigInt& op2);
	friend bool operator<(const BigInt& op1, const BigInt& op2);
	friend bool operator>(const BigInt& op1, const BigInt& op2);
	friend bool operator<=(const BigInt& op1, const BigInt& op2);
	friend bool operator>=(const BigInt& op1, const BigInt& op2);

	BigInt& operator&=(const BigInt& other);
	BigInt& operator|=(const BigInt& other);
	BigInt& operator^=(const BigInt& other);

	friend BigInt operator&(const BigInt& a, const BigInt& b);
	friend BigInt operator|(const BigInt& a, const BigInt& b);
	friend BigInt operator^(const BigInt& a, const BigInt& b);
	friend BigInt operator~(const BigInt& a);

	BigInt& operator<<=(const BigInt& other);
	BigInt& operator>>=(const BigInt& other);

	friend BigInt operator<<(const BigInt& a, const BigInt& b);
	friend BigInt operator>>(const BigInt& a, const BigInt& b);

	static std::string ToStringFormatted(uint64_t num);

	friend std::ostream& operator<<(std::ostream&, const BigInt&);

private:
	bool negSign;
	data_t data;

	short CompareAbs(const BigInt& other) const;
	bool XOR(const bool op1, const bool op2) const;
	void Sum(const BigInt& other);
	void Sub(const BigInt& other);
	void Product(const BigInt& other);
	BigInt Division(const BigInt& other);

	BigInt Multiply(const BigInt& other);
	BigInt PaesantMultiply(const BigInt& other);
	BigInt PaesantMultiplicationStep(const BigInt& op1, const BigInt& op2);

	BigInt CyclicSubtraction(const BigInt& other);

	void ShiftLeft(uint32_t n);
	void ShiftRight(uint32_t n);
};

